@extends('layouts.app')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <div class="container">
            <h1 class="h2">{{ __('USUARIOS') }}</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <button class="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#ModalNew">
                    <img src="{{ asset('feather/plus-circle.svg') }}">
                    {{ __('NUEVO USUARIO') }}
                </button>
            </div>
        </div>
    </div>
    <div class="container">
        <form action="{{ url('/Search/User') }}" method="GET">
            <div class="jumbotron" style="background-color: #fff; border-style: groove; border-color: #E9E9E9; border-width:5px;">
                <div class="panel panel-header">
                    <div class="panel-body">
                        <div class="form-row">
                            <div class="col-md-4 mb3">
                                <label>{{ __('NOMBRE') }}</label>
                                <input type="text" class="form-control" name="name" id="name_list" value="{{ $name }}">
                            </div>
                            <div class="col-md-4 mb3">
                                <label>{{ __('APELLIDO') }}</label>
                            <input type="text" class="form-control" name="last_name" id="last_name_list" value="{{ $last_name }}">
                            </div>
                            <div class="col-md-4 mb3">
                                <label>{{ __('CORREO ELECTRONICO') }}</label>
                                <input type="email" class="form-control" name="email" id="email_list" value="{{ $email }}">
                            </div>
                        </div>                    
                    </div>
                </div>
                <div>
                    <br>
                </div>
                <div class="btn-toolbar mb-2 mb-md-0">                    
                    <button type="button" class="btn btn-outline-primary" title="LIMPIAR FILTROS DE BUSQUEDA" onclick="Clear();">
                        <img src="{{ asset('feather/trash.svg') }}">
                    </button>
                    <button type="submit" class="btn btn-outline-success">                        
                        <img src="{{ asset('feather/search.svg') }}">
                        {{ ('BUSCAR') }}
                    </button>
                </div>
            </div>
        </form>
        <h4>
            {{ ('HAY') }} {{ $user->total() }}
            @if($user->total() == '1')
                {{ __('USUARIO') }}
            @else        
                {{ __('USUARIOS') }}
            @endif
            {{ __('EN TOTAL') }}
        </h4>
        <div class="table-responsive">
            <table class="table table-hover table-sm">
                <thead>
                    <tr>
                        <th><center>{{ __('APELLIDO') }}</center></th>
                        <th><center>{{ __('NOMBRE') }}</center></th>
                        <th><center>{{ __('CORREO') }}</center></th>
                        <th><center>{{ __('TELEFONO') }}</center></th>
                        <th><center>{{ __('DIRECCIÓN') }}</center></th>
                        <th><center>{{ __('ACCIONES') }}</center></th>
                    </tr>
                </thead>
                <tbody id="tableuser">
                    @foreach ($user as $users)
                        <tr>
                            <td class="hoverable"><center>{{$users->last_name}}</center></td>
                            <td class="hoverable"><center>{{$users->name}}</center></td>
                            <td class="hoverable"><center>{{$users->email}}</center></td>
                            <td class="hoverable"><center>{{$users->cel}}</center></td>
                            <td class="hoverable"><center>{{$users->dir}}</center></td>
                            <td class="hoverable">
                                <center>
                                    <button value="{{ $users->id }}" class="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#ModalUpdate" id="UpdateStaus" onClick="Mostrar(this);" title="EDITAR">
                                        <img src="{{ asset('feather/edit.svg') }}">
                                    </button>
                                    <a href="/User/{{ $users->id }}/delete" class="btn btn-sm btn-outline-danger" onclick="return confirm('ESTA SEGURO DE ELIMINAR EL USUARIO');" title="ELIMINAR USUARIO">
                                        <img src="{{ asset('feather/trash-2.svg') }}">
                                    </a>
                                </center>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="pull-right">
            {{ $user->appends(Request::capture()->except('page'))->render() }}
        </div>
    </div>
    {{ csrf_field() }}
    @include('User/Modals.new')
    @include('User/Modals.update')
    <script type="text/javascript" src="{{ asset('/js/User/list.js') }}"></script>
@endsection
        