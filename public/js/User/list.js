// function to clean the search filters
function Clear()
{
    $('#name_list').val('');    
	$('#last_name_list').val('');
	$('#email_list').val('');
	alertify.set('notifier', 'position', 'top-center');
	alertify.success('LOS CAMPOS FUERON LIMPIADOS CORRECTAMENTE', 8);
}