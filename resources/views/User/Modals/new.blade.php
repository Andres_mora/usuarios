<!-- Modal -->
<div class="modal fade" id="ModalNew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <!-- Colocamos un input oculto con el token -->
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
          <h4 class="modal-title" id="myModalLabel"><center>NUEVO USUARIO</center></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label>{{ ('APELLIDO') }}</label>
                    <input type="text" class="form-control" id="last_name_new" style="text-transform:uppercase;"onkeyup="javascript:this.value=this.value.toUpperCase();"/>
                </div>
                <div class="col-md-4 mb-3">
                    <label>{{ ('NOMBRE') }}</label>
                    <input type="text" class="form-control" id="name_new" style="text-transform:uppercase;"onkeyup="javascript:this.value=this.value.toUpperCase();"/>
                </div>
                <div class="col-md-4 mb-3">
                    <label>{{ ('CORREO') }}</label>
                    <input type="text" class="form-control" id="email_new" style="text-transform:uppercase;"onkeyup="javascript:this.value=this.value.toUpperCase();"/>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label>{{ ('TELEFONO') }}</label>
                    <input type="number" class="form-control" id="cel_new" style="text-transform:uppercase;"onkeyup="javascript:this.value=this.value.toUpperCase();"/>
                </div>
                <div class="col-md-6 mb-3">
                    <label>{{ ('DIRECCIÓN') }}</label>
                    <input type="text" class="form-control" id="dir_new" style="text-transform:uppercase;"onkeyup="javascript:this.value=this.value.toUpperCase();"/>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label>{{ ('CONTRASEÑA') }}</label>
                    <input type="password" class="form-control" id="password_new" style="text-transform:uppercase;"onkeyup="javascript:this.value=this.value.toUpperCase();"/>
                </div>
                <div class="col-md-6 mb-3">
                    <label>{{ ('CONFIRMAR CONTRASEÑA') }}</label>
                    <input type="password" class="form-control" id="password_confirmation_new" style="text-transform:uppercase;"onkeyup="javascript:this.value=this.value.toUpperCase();"/>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">{{ __('CANCELAR') }}</button>
          <button type="button" class="btn btn-outline-success" id="newUser" onclick="NEW();">{{ __('RESGISTRAR') }}</button>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="{{ asset('js/User/new.js') }}"></script>