function Mostrar(btn)
{
	var route = '/User/'+btn.value+'/edit';

	$.get(route, function(res)
	{
        $('#last_name_edit').val(res.last_name);
        $('#name_edit').val(res.name);
        $('#email_edit').val(res.email);
        $('#cel_edit').val(res.cel);
        $('#dir_edit').val(res.dir);
		$('#id').val(res.id);
	});
}

function Update()
{
	var id = $('#id').val();
	
	var last_name = $('#last_name_edit').val();
	var name = $('#name_edit').val();
	var email = $('#email_edit').val();
	var cel = $('#cel_edit').val();
	var dir = $('#dir_edit').val();
	var password = $('#password_edit').val();
    var password_confirmation = $('#password_confirmation_edit').val();

	var route = '/User/'+id+'';
	var token = $('#token').val();

	if (password != password_confirmation)
    {
        alertify.set('notifier', 'position', 'top-center');
        alertify.error('LAS CONTRASEÑAS NO COINCIDEN', 8);
    }
    else
	{
		$.ajax(
		{
			url: route,
			headers:
			{
				'X-CSRF-TOKEN': token
			},
			type: 'PUT',
			dataType: 'json',
			data:
			{	
                last_name: last_name,
                name: name,
                email: email,
                cel: cel,
                dir: dir,
                password: password,
			},
			success:function()
			{
				alertify.set('notifier','position', 'top-center');
				alertify.success('LOS CAMBIOS HAN SIDO GUARDADOS EXITOSAMENTE', 8);
				location.reload();
			},
			error: function (data)
			{
				alertify.set('notifier','position', 'top-center');
				alertify.error('POR FAVOR REVISA LA INFORMACION INSERTADA', 5);
				alertify.error('ALGO HA SALIDO MAL ...', 5);
		        console.log('Error:', data);
		    }
		});
	}
}    