<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $name = null;
        $last_name = null;
        $email = null;
        
        $user = User::orderby('last_name')
                        ->paginate(1);

        return view('User.list')->with(compact('user', 'name', 'last_name', 'email'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('User.Modals.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax())
        {
            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = $request->password;
            $user->last_name = $request->last_name;
            $user->cel = $request->cel;
            $user->dir = $request->dir;
            $user->save();

            return response()->json([
                'mensaje' => 'Informacion guardada',
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return response()->json(
            $user->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->ajax())
        {
            $user = User::find($id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = $request->password;
            $user->last_name = $request->last_name;
            $user->cel = $request->cel;
            $user->dir = $request->dir;
            $user->save();

            return response()->json([
                'mensaje' => 'Informacion actualizada',
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return back();
    }

    /**
     * Funcion de busqueda
     */
    public function search(Request $request)
    {
        $name = $request->name;
        $last_name = $request->last_name;
        $email = $request->email;
        
        if($name != null)
        {
            $user = User::where('name', 'LIKE', '%'.$name.'%')
                            ->orderby('last_name')
                            ->paginate(1);
        }
        elseif ($last_name != null)
        {
            $user = User::where('last_name', 'LIKE', '%'.$last_name.'%')
                            ->orderby('last_name')
                            ->paginate(1);
        }
        elseif ($email != null)
        {
            $user = User::where('email', 'LIKE', '%'.$email.'%')
                            ->orderby('last_name')
                            ->paginate(1);
        }
        else
        {
            $user = User::orderby('last_name')
                            ->paginate(1);
        }

        return view('User.list')->with(compact('user', 'name', 'last_name', 'email'));
    }
}
