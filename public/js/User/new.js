function NEW()
{
	var last_name = $('#last_name_new').val();
	var name = $('#name_new').val();
	var email = $('#email_new').val();
	var cel = $('#cel_new').val();
	var dir = $('#dir_new').val();
	var password = $('#password_new').val();
    var password_confirmation = $('#password_confirmation_new').val();
    
	var route = '/User';
	var token = $('#token').val();


    if (password != password_confirmation)
    {
        alertify.set('notifier', 'position', 'top-center');
        alertify.error('LAS CONTRASEÑAS NO COINCIDEN', 8);
    }
    else
    {
        $.ajax(
        {
            url: route,
            headers:
            {
                'X-CSRF-TOKEN': token
            },
            type: 'POST',
            dataType: 'json',
            data:
            {
                last_name: last_name,
                name: name,
                email: email,
                cel: cel,
                dir: dir,
                password: password,
            },
            success:function()
            {
                alertify.set('notifier', 'position', 'top-center');
                alertify.success('EL USUARIO: '+last_name+' '+name+' FUE REGISTRADO CORRECTAMENTE', 8);
                location.reload();
            },
            error: function (data)
            {     
            alertify.set('notifier','position', 'top-center');
            alertify.error('POR FAVOR REVISA LA INFORMACION INSERTADA', 5);
            alertify.error('ALGO HA SALIDO MAL ...', 5);
                console.log('Error:', data);
            }
        });
    }
}